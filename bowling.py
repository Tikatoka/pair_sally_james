class Score_Calculator():
    def __init__(self):
        self.frame_index = 0
        self.bonus_record = {}
        self.total_score = 0
    def processFrame(self):
        current_frame = self.frame_index
        score_for_frame = 0
        bonus_score = 0
        for i in range(2):
            score = int(input('Please input the score for roll-' + str(i+1) + ': '))
            if score == 10:
                self.bonus_record[current_frame] = 2
            score_for_frame += score

            if score_for_frame == 10:
                self.bonus_record[current_frame] = 2 - i

            if (current_frame - 1) in self.bonus_record and self.bonus_record[current_frame - 1] > 0:
                self.bonus_record[current_frame - 1] -= 1
                bonus_score += score

            if (current_frame - 2) in self.bonus_record and self.bonus_record[current_frame - 2] > 0:
                self.bonus_record[current_frame - 2] -= 1
                bonus_score += score
            if score == 10:
                break
        return score_for_frame + bonus_score

    def processNextFrame(self):
        while True:
            self.frame_index += 1
            score_frame = self.processFrame()
            self.total_score += score_frame
            print('Current Total Score is: ', self.total_score)

if __name__ == '__main__':
    cal = Score_Calculator()
    cal.processNextFrame()